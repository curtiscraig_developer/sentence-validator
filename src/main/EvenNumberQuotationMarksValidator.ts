export class EvenNumberQuotationMarksValidator {
  static sentenceHasEvenNumberQuotationMarks(sentence: string): boolean {
    const numberQuotationMarks = this.countCharacterOccurrences(sentence, '"');
    const evenNumberQuotationMarks =
      numberQuotationMarks == 0 || numberQuotationMarks % 2 == 0;

    console.log(
      `Sentence contains even number of quotation marks: ${evenNumberQuotationMarks}`
    );

    return evenNumberQuotationMarks;
  }

  private static countCharacterOccurrences(
    sentence: string,
    characterToCount: string
  ): number {
    const arrayFromString = Array.from(sentence);
    let counter = 0;

    for (let x = 0; x < arrayFromString.length; x++) {
      if (arrayFromString[x].valueOf() === characterToCount) {
        counter += 1;
      }
    }

    return counter;
  }
}
