import { StartsWithCapitalValidator } from './StartsWithCapitalValidator';
import { EndsWithValidCharacterValidator } from './EndsWithValidCharacterValidator';
import { NoPeriodBeforeLastCharacter } from './NoPeriodBeforeLastCharacter';
import { EvenNumberQuotationMarksValidator } from './EvenNumberQuotationMarksValidator';
import { NumbersBelowThirteenSpelledOut } from './NumbersBelowThirteenSpelledOut';

export class sentenceValidator {
  static isValidSentence(sentence: string): boolean {
    const validSentence =
      StartsWithCapitalValidator.sentenceStartsWithCapital(sentence) &&
      EvenNumberQuotationMarksValidator.sentenceHasEvenNumberQuotationMarks(
        sentence
      ) &&
      EndsWithValidCharacterValidator.sentenceEndsWithValidCharacter(
        sentence
      ) &&
      NoPeriodBeforeLastCharacter.sentenceHasNoPeriodBeforeLastCharacter(
        sentence
      ) &&
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentence
      );

    console.log(`Valid sentence: ${validSentence}`);
    return validSentence;
  }
}
