export class StartsWithCapitalValidator {
  static sentenceStartsWithCapital(sentence: string): boolean {
    const startsWithCapital =
      sentence.charCodeAt(0) >= 65 &&
      sentence.charCodeAt(0) <= 90 &&
      sentence.charCodeAt(0) === sentence.toUpperCase().charCodeAt(0);

    console.log(`Sentence starts with capital: ${startsWithCapital} `);

    return startsWithCapital;
  }
}
