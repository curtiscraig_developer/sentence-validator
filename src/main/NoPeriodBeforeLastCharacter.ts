export class NoPeriodBeforeLastCharacter {
  static sentenceHasNoPeriodBeforeLastCharacter(sentence: string): boolean {
    const arrayFromString = Array.from(sentence);
    let noFullStopBeforeEnd = true;

    for (let x = 0; x < arrayFromString.length; x++) {
      if (
        arrayFromString[x].valueOf() === '.' &&
        x < arrayFromString.length - 1
      )
        noFullStopBeforeEnd = false;
    }
    console.log(`No full stop before end: ${noFullStopBeforeEnd}`);
    return noFullStopBeforeEnd;
  }
}
