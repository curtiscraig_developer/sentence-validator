export class EndsWithValidCharacterValidator {
  static sentenceEndsWithValidCharacter(sentence: string): boolean {
    const lastCharacter = sentence.charAt(sentence.length - 1);
    let endsWithValidCharacter = false;

    switch (lastCharacter) {
      case '!':
      case '.':
      case '?':
        endsWithValidCharacter = true;
    }

    console.log(
      `Sentence ends with valid character: ${endsWithValidCharacter}`
    );

    return endsWithValidCharacter;
  }
}
