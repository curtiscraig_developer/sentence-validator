export class NumbersBelowThirteenSpelledOut {
  static sentencesWithNumbersBelowThirteenAreSpelledOut(
    sentence: string
  ): boolean {
    const arrayFromString = Array.from(sentence);
    let numbersBelowThirteenSpelledOut = true;

    for (let x = 0; x < arrayFromString.length; x++) {
      const previousChar = sentence.charCodeAt(x - 1);
      const nextChar = sentence.charCodeAt(x + 1);
      const afterNextChar = sentence.charCodeAt(x + 2);

      if (this.isNumber(previousChar)) continue;
      else {
        switch (sentence.charAt(x)) {
          case '0':
            numbersBelowThirteenSpelledOut = false;
            break;
          case '1':
            if (this.isNumberLessThanThirteen(nextChar, afterNextChar))
              numbersBelowThirteenSpelledOut = false;
            break;
          case '2':
          case '3':
          case '4':
          case '5':
          case '6':
          case '7':
          case '8':
          case '9':
            if (!this.isNumber(nextChar))
              numbersBelowThirteenSpelledOut = false;
            break;
        }
      }
    }
    console.log(
      `Numbers below thirteen are spelled out: ${numbersBelowThirteenSpelledOut}`
    );
    return numbersBelowThirteenSpelledOut;
  }
  /*
This method will check if previous character is a number.
If it is, we will not check the numbers after it, as it is part of a number already being checked.
 */
  private static isNumber(charCode: number): boolean {
    let isNumber = false;
    if (charCode >= 48 && charCode <= 57) {
      isNumber = true;
    }

    return isNumber;
  }

  /*
  This will be called when the character is 1.
  It carries out a number of checks:
  First, it checks if the next character is 0, 1, or 2 and that it isn't followed by another digit.
  It will also return true if the next character is a non-numeric.
  Both these cases mean the number is less than 13.
   */
  private static isNumberLessThanThirteen(
    charCode: number,
    nextCharCode: number
  ): boolean {
    let lessThanThirteen = false;
    if (
      (this.isNumberBetweenZeroAndTwo(charCode) &&
        !this.isNumber(nextCharCode)) ||
      !this.isNumber(charCode)
    )
      lessThanThirteen = true;
    return lessThanThirteen;
  }

  /*
This checks if a number is between zero and two.
 */
  private static isNumberBetweenZeroAndTwo(charCode: number): boolean {
    let isBetweenZeroAndTwo = false;
    if (charCode >= 48 && charCode <= 50) isBetweenZeroAndTwo = true;
    return isBetweenZeroAndTwo;
  }
}
