import { EndsWithValidCharacterValidator } from '../main/EndsWithValidCharacterValidator';

describe('sentenceEndsWithValidCharacter', () => {
  it('should return true when sentence ends with a full stop', () => {
    const sentenceEndingWithFullStop =
      'The quick brown fox said "hello mr lazy dog.';
    expect(
      EndsWithValidCharacterValidator.sentenceEndsWithValidCharacter(
        sentenceEndingWithFullStop
      )
    ).toBeTruthy();
  });

  it('should return true when sentence ends with a question mark', () => {
    const sentenceEndingWithQuestionMark =
      'The quick brown fox said "hello mr lazy dog?';
    expect(
      EndsWithValidCharacterValidator.sentenceEndsWithValidCharacter(
        sentenceEndingWithQuestionMark
      )
    ).toBeTruthy();
  });

  it('should return true when sentence ends with an exclamation mark', () => {
    const sentenceEndingWithExclamationMark =
      'The quick brown fox said "hello mr lazy dog!';
    expect(
      EndsWithValidCharacterValidator.sentenceEndsWithValidCharacter(
        sentenceEndingWithExclamationMark
      )
    ).toBeTruthy();
  });

  it('should return false when sentence ends with invalid character', () => {
    const sentenceEndingWithExclamationMark =
      'The quick brown fox said "hello mr lazy dog';
    expect(
      EndsWithValidCharacterValidator.sentenceEndsWithValidCharacter(
        sentenceEndingWithExclamationMark
      )
    ).toBeFalsy();
  });
});
