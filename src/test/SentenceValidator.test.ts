import { sentenceValidator } from '../main/SentenceValidator';

describe('validateSentence', () => {
  it('should return true when given a valid sentence', () => {
    const validSentence = 'The quick brown fox said "hello Mr lazy dog".';
    expect(sentenceValidator.isValidSentence(validSentence)).toBeTruthy();
  });

  it('should return true when given a valid sentence', () => {
    const validSentence = 'The quick brown fox said hello Mr lazy dog.';
    expect(sentenceValidator.isValidSentence(validSentence)).toBeTruthy();
  });

  it('should return true when given a valid sentence', () => {
    const validSentence = 'One lazy dog is too few, 13 is too many.';
    expect(sentenceValidator.isValidSentence(validSentence)).toBeTruthy();
  });

  it('should return true when given a valid sentence', () => {
    const validSentence = 'One lazy dog is too few, thirteen is too many.';
    expect(sentenceValidator.isValidSentence(validSentence)).toBeTruthy();
  });

  it('should return true when given a valid sentence', () => {
    const validSentence = 'How many "lazy dogs" are there?';
    expect(sentenceValidator.isValidSentence(validSentence)).toBeTruthy();
  });

  it('should return false when given an invalid sentence', () => {
    const invalidSentence = 'The quick brown fox said "hello Mr. lazy dog"';
    expect(sentenceValidator.isValidSentence(invalidSentence)).toBeFalsy();
  });

  it('should return false when given an invalid sentence', () => {
    const invalidSentence = 'the quick brown fox said "hello Mr lazy dog".';
    expect(sentenceValidator.isValidSentence(invalidSentence)).toBeFalsy();
  });

  it('should return false when given an invalid sentence', () => {
    const invalidSentence = '"The quick brown fox said "hello Mr lazy dog."';
    expect(sentenceValidator.isValidSentence(invalidSentence)).toBeFalsy();
  });

  it('should return false when given an invalid sentence', () => {
    const invalidSentence = 'One lazy dog is too few, 12 is too many.';
    expect(sentenceValidator.isValidSentence(invalidSentence)).toBeFalsy();
  });

  it('should return false when given an invalid sentence', () => {
    const invalidSentence = 'Are there 11, 12, or 13 lazy dogs?';
    expect(sentenceValidator.isValidSentence(invalidSentence)).toBeFalsy();
  });

  it('should return false when given an invalid sentence', () => {
    const invalidSentence = 'There is no punctuation in this sentence';
    expect(sentenceValidator.isValidSentence(invalidSentence)).toBeFalsy();
  });
});
