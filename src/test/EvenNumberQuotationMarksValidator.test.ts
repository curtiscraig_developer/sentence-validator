import { EvenNumberQuotationMarksValidator } from '../main/EvenNumberQuotationMarksValidator';

describe('sentenceHasEvenNumberQuotationMarks', () => {
  it('should return true when a sentence has even number of quotation marks', () => {
    const sentenceWithEvenNumberQuotationMarks =
      'The quick brown fox said "hello mr lazy dog"';
    expect(
      EvenNumberQuotationMarksValidator.sentenceHasEvenNumberQuotationMarks(
        sentenceWithEvenNumberQuotationMarks
      )
    ).toBeTruthy();
  });

  it('should return false when a sentence has odd number of quotation marks', () => {
    const sentenceWithOddNumberQuotationMarks =
      '"The quick brown fox said "hello mr lazy dog"';
    expect(
      EvenNumberQuotationMarksValidator.sentenceHasEvenNumberQuotationMarks(
        sentenceWithOddNumberQuotationMarks
      )
    ).toBeFalsy();
  });
});
