import { NoPeriodBeforeLastCharacter } from '../main/NoPeriodBeforeLastCharacter';

describe('sentenceHasNoPeriodsBeforeLastCharacter', () => {
  it('should return true when sentence does not have full stop before last character', () => {
    const sentenceWithNoFullStopsExceptLastCharacter =
      'The quick brown fox said "hello mr lazy dog.';
    expect(
      NoPeriodBeforeLastCharacter.sentenceHasNoPeriodBeforeLastCharacter(
        sentenceWithNoFullStopsExceptLastCharacter
      )
    ).toBeTruthy();
  });

  it('should return false when sentence has full stop before last character', () => {
    const sentenceWithFullStopsBeforeLastCharacter =
      'The quick brown fox. said "hello mr lazy dog.';
    expect(
      NoPeriodBeforeLastCharacter.sentenceHasNoPeriodBeforeLastCharacter(
        sentenceWithFullStopsBeforeLastCharacter
      )
    ).toBeFalsy();
  });
});
