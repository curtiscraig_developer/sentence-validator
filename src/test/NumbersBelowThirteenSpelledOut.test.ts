import { NumbersBelowThirteenSpelledOut } from '../main/NumbersBelowThirteenSpelledOut';

describe('numbersBelowThirteenAreSpelledOut', () => {
  it('should return false when 0 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 0 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return false when 1 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 1 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return false when 2 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 2 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return false when 3 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 3 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return false when 4 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 4 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return false when 5 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 5 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return false when 6 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 6 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return false when 7 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 7 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return false when 8 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 8 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return false when 9 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 9 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return false when 10 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 10 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return false when 11 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 11 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return false when 12 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 12 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return true when 13 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 13 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeTruthy();
  });

  it('should return true when 46 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 46 lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeTruthy();
  });

  it('should return true when 100 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 100 dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeTruthy();
  });

  it('should return true when 120 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 120 dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeTruthy();
  });

  it('should return true when 130 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 130 dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeTruthy();
  });

  it('should return true when 12 and 130 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 12 or 130 dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return true when 130 and 8 appears in sentence', () => {
    const sentenceWithNumberNotSpelledOut = 'Are there 130 or 8 dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberNotSpelledOut
      )
    ).toBeFalsy();
  });

  it('should return true when thirteen appears in sentence', () => {
    const sentenceWithNumberSpelledOut = 'Are there thirteen lazy dogs?';
    expect(
      NumbersBelowThirteenSpelledOut.sentencesWithNumbersBelowThirteenAreSpelledOut(
        sentenceWithNumberSpelledOut
      )
    ).toBeTruthy();
  });
});
