import { StartsWithCapitalValidator } from '../main/StartsWithCapitalValidator';

describe('sentenceStartsWithCapital', () => {
  test('should return true when sentence begins with capital letter', () => {
    const sentenceWithCapitalAtStart = 'The quick brown fox';
    expect(
      StartsWithCapitalValidator.sentenceStartsWithCapital(
        sentenceWithCapitalAtStart
      )
    ).toBeTruthy();
  });

  test('"the quick brown fox" should return false', () => {
    const sentenceWithLowercaseAtStart = 'the quick brown fox';
    expect(
      StartsWithCapitalValidator.sentenceStartsWithCapital(
        sentenceWithLowercaseAtStart
      )
    ).toBeFalsy();
  });
});
