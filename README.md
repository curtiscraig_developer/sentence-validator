# README #

This is a project designed to validate a sentence based on a set number of rules. 
It will be completed using Nodejs and Typescript.

### What is this repository for? ###

* Sentence Validator as part of an interview.

### How do I get set up? ###

Install the packages needed using npm install which will install the dependencies required, listed in the package.json.
'npm run start' - everything you need to start
'npm run build-test' - this will run the build and tests
'npm run build' - this will run a build
'npm run test' - this will run the tests

### Who do I talk to? ###

* Curtis Craig (curtiscraig_developer@outlook.com)